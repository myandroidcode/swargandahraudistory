import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';

import 'app/modules/mainscreen/views/mainscreen_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init('MyPref');
  runApp(MainScreenView());
}
