import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appTextStyles.dart';

class TextInputLayout extends StatelessWidget {
  final ValueChanged<String>? onFieldSubmitted;
  final TextEditingController? controller;
  final String? hintText;
  final String? labelText;
  final Icon? prefixIcon;
  final Icon? suffixIcon;
  final Widget? suffixWidget;
  final FormFieldValidator<String>? validator;
  final int? maxLength;
  final bool obscureText;
  final TextInputType? keyboardType;
  final AutovalidateMode? autoValidateMode;
  final TextInputAction? textInputAction;
  final FormFieldSetter<String>? onSaved;
  final bool? enabled;

  TextInputLayout(
      {Key? key,
      this.onFieldSubmitted,
      this.controller,
      this.hintText,
      this.labelText,
      this.prefixIcon,
      this.validator,
      this.maxLength,
      this.obscureText = false,
      this.keyboardType,
      this.autoValidateMode,
      this.textInputAction,
      this.onSaved,
      this.enabled,
      this.suffixIcon,
      this.suffixWidget})
      : super(key: key);

//   @override
//   State<StatefulWidget> createState() => TextInputLayoutState(
//       this.onFieldSubmitted,
//       this.controller,
//       this.hintText,
//       this.labelText,
//       this.prefixIcon,
//       this.validator,
//       this.maxLength,
//       this.obscureText,
//       this.keyboardType,
//       this.autoValidateMode,
//       this.textInputAction);
// }

// class TextInputLayoutState extends State<TextInputLayout> {
//   final ValueChanged<String>? onFieldSubmitted;
//   final TextEditingController? controller;
//   final String? hintText;
//   final String? labelText;
//   final Icon? prefixIcon;
//   final FormFieldValidator<String>? validator;
//   final int? maxLength;
//   final bool? obscureText;
//   final TextInputType? keyboardType;
//   final AutovalidateMode? autoValidateMode;
//   final TextInputAction? textInputAction;
//
//   TextInputLayoutState(
//       this.onFieldSubmitted,
//       this.controller,
//       this.hintText,
//       this.labelText,
//       this.prefixIcon,
//       this.validator,
//       this.maxLength,
//       this.obscureText,
//       this.keyboardType,
//       this.autoValidateMode,
//       this.textInputAction);
//
//   @override
//   void initState() {}
//
//   @override
//   void dispose() {
//     // focusNode.dispose();
//     super.dispose();
//   }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8),
      child: TextFormField(
        textInputAction: textInputAction,
        onFieldSubmitted: onFieldSubmitted,
        autovalidateMode: autoValidateMode,
        controller: controller,
        obscureText: obscureText,
        enabled: enabled ?? true,
        style: AppTextStyles().kTextStyleWithFont,
        keyboardType: keyboardType,
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: AppTextStyles().kTextStyleWithFont,
          labelText: labelText,
          labelStyle: AppTextStyles().kTextStyleWithFont,
          prefixIcon: prefixIcon,
          suffixIcon: suffixIcon,
          suffix: suffixWidget,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
          isDense: true,
          errorStyle: AppTextStyles().kTextStyleWithFont,
        ),
        maxLength: maxLength,
        validator: validator,
        onSaved: onSaved,
      ),
    );
  }
}
