import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appTextStyles.dart';

class AppBarText extends StatelessWidget {
  final String? title;

  const AppBarText({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        title!,
        style: AppTextStyles().appBarThemeColor,
      ),
    );
  }
}
