import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/customview/text/title_text.dart';
import 'package:swargandhar/app/modules/network/controllers/network_controller.dart';

class CustomScaffold extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget? body;
  final Widget? floatingActionButton;
  final Color? backgroundColor;
  final Widget? drawer;

  CustomScaffold({
    Key? key,
    this.appBar,
    this.body,
    this.floatingActionButton,
    this.backgroundColor,
    this.drawer,
  }) : super(key: key);

  final NetworkController _networkController = Get.put(NetworkController());

  @override
  Widget build(BuildContext context) {
    return ThemeSwitchingArea(
      child: Scaffold(
        appBar: appBar,
        drawer: drawer,
        body: SafeArea(
            child: Column(
          children: [
            Visibility(
              visible: _networkController.getNetworkStatus() != 'Connected',
              child: Expanded(
                flex: 0,
                child: Obx(
                  () => Container(
                    child: TitleText(
                      text: _networkController.getNetworkStatus(),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(child: body!)
          ],
        )),
        floatingActionButton: floatingActionButton,
        backgroundColor: Theme.of(context).colorScheme.background,
        resizeToAvoidBottomInset: true,
      ),
    );
  }
}
