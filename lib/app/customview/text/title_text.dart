import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appTextStyles.dart';

class TitleText extends StatelessWidget {
  final String? text;
  final Color? color;
  final double? textSize;
  final AlignmentGeometry? alignment;
  final TextAlign? textAlign;
  final EdgeInsetsGeometry? margin;
  final TextOverflow? overflow;
  final double? width;
  final int? maxLines;
  final double? lineSpacing;

  const TitleText(
      {Key? key,
      this.text,
      this.color,
      this.textSize,
      this.alignment,
      this.textAlign,
      this.margin,
      this.overflow,
      this.width,
      this.maxLines,
      this.lineSpacing})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: key,
      margin: margin,
      alignment: alignment,
      width: width,
      child: Text(
        text ?? '',
        style: AppTextStyles().titleStyle(fontSize: textSize),
        textAlign: textAlign,
        overflow: overflow ?? TextOverflow.ellipsis,
        maxLines: maxLines,
      ),
    );
  }
}
