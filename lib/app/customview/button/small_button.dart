import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appTextStyles.dart';

class SmallButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String? text;
  final EdgeInsetsGeometry? padding;
  final Color? color;
  final EdgeInsetsGeometry? margin;
  final double? fontSize;
  final double? height;

  SmallButton(
      {Key? key,
      this.onPressed,
      this.text,
      this.padding,
      this.color,
      this.margin,
      this.fontSize,
      this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42,
      padding: padding,
      margin: EdgeInsets.only(top: 10),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Text(
          text!,
          style: AppTextStyles().kTextStyleTwelveWithGreyColor,
        ),
      ),
    );
  }
}
