import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appTextStyles.dart';

class LargeButton extends StatelessWidget {
  @required
  final VoidCallback? onPressed;
  final String? text;
  final Key? key;
  final Color? color;
  final EdgeInsetsGeometry? margin;
  final ShapeBorder? shape;

  const LargeButton({
    this.key,
    this.onPressed,
    this.text,
    this.margin,
    this.shape,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      margin: margin == null ? EdgeInsets.only(top: 14.0) : margin,
      width: double.maxFinite,
      child: ElevatedButton(
        key: key,
        onPressed: onPressed,
        child: Text(
          text!.toUpperCase(),
          style: AppTextStyles().kTextStyleWithFont,
        ),
        style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(50),
              ),
            ),
            elevation: 10,
            padding: EdgeInsets.all(14)),
      ),
    );
  }
}
