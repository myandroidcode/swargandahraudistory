import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:swargandhar/app/customview/input/text_inout_layout.dart';

class CustomDropDown<T> extends StatelessWidget {
  final TextEditingController? controller;
  final PopupMenuItemSelected<T>? onSelected;

  final list;

  const CustomDropDown({Key? key, this.controller, this.onSelected, this.list})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerRight,
      children: <Widget>[
        TextInputLayout(
          controller: controller,
          enabled: false,
        ),
        showPopUip(context)
      ],
    );
  }

  showPopUip(BuildContext context) {
    return PopupMenuButton<T>(
      icon: Icon(
        Icons.arrow_drop_down,
        color: Theme.of(context).primaryColor,
      ),
      onSelected: onSelected,
      itemBuilder: (BuildContext context) {
        return list;
      },
    );
  }
}

/*
example  use this in your view
Locale -> replace with your model
 CustomDropDown<Locale>(
                  onSelected: (local) {
                    print(
                        'selectedValue: ${local.languageCode} and ${local.countryCode}');
                  },
                  controller: _controller,
                  list: localList
                      .map((Locale l) => PopupMenuItem<Locale>(
                          value: l,
                          child: ListTile(
                            title: Text(l.languageCode),
                            subtitle: Text(l.countryCode!),
                          )))
                      .toList(),
                ),
 */

/*
* use the list
* replace Locale with your model
* List<Locale> localList = [];
  localList.add(Locale('Marathi', 'India'));
  localList.add(Locale('hindi', 'India'));
  localList.add(Locale('hindi', 'India'));
 */
