import 'package:get/get.dart';

class AppTranslation extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_IN': {
          'login_screen': 'Login',
          'Home': 'Home',
          'select_language': 'Select Language',
          'login': 'Login',
          'enter_mobile': 'Enter Mobile Number and Password to Login',
          'register': 'Click here to register',
          'continue': 'Continue',
          'mobile_no': 'Mobile Number',
          'password': 'Password',
          'sign_up': 'Sign up',
          'enter_name': 'Enter Name',
          'enter_email': 'Enter email',
          'registration': 'registration',
          'confirm_password': 'Confirm password'
        },
        'hi_IN': {
          'login_screen': 'लॉगिन',
          'Home': 'घर',
          'select_language': 'भाषा का चयन करें',
          'login': 'लॉग इन करें',
          'enter_mobile': 'लॉगिन करने के लिए मोबाइल नंबर और पासवर्ड दर्ज करें',
          'register': 'रजिस्टर करने के लिए यहाँ क्लिक करें',
          'continue': 'जारी रखें',
          'mobile_no': 'मोबाइल नंबर',
          'password': 'पासवर्ड',
          'sign_up': 'साइन अप करें',
          'enter_name': 'नाम दर्ज करें',
          'enter_email': 'ईमेल दर्ज करें',
          'registration': 'पंजीकरण',
          'confirm_password': 'पासवर्ड की पुष्टि कीजिये'
        },
        'mr_IN': {
          'login_screen': 'लॉग इन करें ',
          'Home': 'मुख्यपृष्ठ',
          'select_language': 'भाषा निवडा',
          'login': 'लॉगिन',
          'enter_mobile':
              'लॉगिन करण्यासाठी मोबाइल नंबर आणि संकेतशब्द प्रविष्ट करा',
          'register': 'नोंदणी करण्यासाठी येथे क्लिक करा',
          'continue': 'सुरू',
          'mobile_no': 'मोबाइल नंबर',
          'password': 'संकेतशब्द',
          'sign_up': 'साइन अप करा',
          'enter_name': 'नाव प्रविष्ट करा',
          'enter_email': 'ईमेल प्रविष्ट करा',
          'registration': 'नोंदणी',
          'confirm_password': 'पासवर्डची पुष्टी करा'
        }
      };
}
