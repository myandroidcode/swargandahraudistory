part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const LOGIN = _Paths.LOGIN;
  static const MAINSCREEN = _Paths.MAINSCREEN;
  static const SIGN_UP = _Paths.SIGN_UP;
  static const DYNAMICFORM = _Paths.DYNAMICFORM;
  static const ONBOARDING = _Paths.ONBOARDING;
}

abstract class _Paths {
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const MAINSCREEN = '/mainscreen';
  static const SIGN_UP = '/sign-up';
  static const DYNAMICFORM = '/dynamicform';
  static const ONBOARDING = '/onboarding';
}
