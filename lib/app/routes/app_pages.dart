import 'package:get/get.dart';

import 'package:swargandhar/app/modules/SignUp/bindings/sign_up_binding.dart';
import 'package:swargandhar/app/modules/SignUp/views/sign_up_view.dart';
import 'package:swargandhar/app/modules/dynamicform/bindings/dynamicform_binding.dart';
import 'package:swargandhar/app/modules/dynamicform/views/dynamicform_view.dart';
import 'package:swargandhar/app/modules/home/bindings/home_binding.dart';
import 'package:swargandhar/app/modules/home/views/home_view.dart';
import 'package:swargandhar/app/modules/login/bindings/login_binding.dart';
import 'package:swargandhar/app/modules/login/views/login_view.dart';
import 'package:swargandhar/app/modules/mainscreen/bindings/mainscreen_binding.dart';
import 'package:swargandhar/app/modules/mainscreen/views/mainscreen_view.dart';
import 'package:swargandhar/app/modules/onboarding/bindings/onboarding_binding.dart';
import 'package:swargandhar/app/modules/onboarding/views/onboarding_view.dart';
import 'package:swargandhar/app/modules/splash/bindings/splash_binding.dart';
import 'package:swargandhar/app/modules/splash/views/splash_view.dart';

part 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.MAINSCREEN,
      page: () => MainScreenView(),
      binding: MainscreenBinding(),
    ),
    GetPage(
      name: _Paths.SIGN_UP,
      page: () => SignUpView(),
      binding: SignUpBinding(),
    ),
    GetPage(
      name: _Paths.DYNAMICFORM,
      page: () => DynamicformView(),
      binding: DynamicformBinding(),
    ),
    GetPage(
      name: _Paths.ONBOARDING,
      page: () => OnboardingView(),
      binding: OnboardingBinding(),
    ),
  ];
}
