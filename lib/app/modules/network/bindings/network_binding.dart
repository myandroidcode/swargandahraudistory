import 'package:get/get.dart';
import 'package:swargandhar/app/modules/network/controllers/network_controller.dart';

class NetworkBinding extends Bindings {
  @override
  void dependencies() {
    // Get.put(NetworkController(), permanent: true);
    Get.lazyPut<NetworkController>(() => NetworkController(), fenix: true);
  }
}
