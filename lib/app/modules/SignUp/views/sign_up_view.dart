import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/constants/color.dart';
import 'package:swargandhar/app/constants/top_image.dart';
import 'package:swargandhar/app/customview/appbar/app_bar_text.dart';
import 'package:swargandhar/app/customview/appbar/custom_scaffold.dart';
import 'package:swargandhar/app/customview/button/large_button.dart';
import 'package:swargandhar/app/customview/input/text_inout_layout.dart';
import 'package:swargandhar/app/customview/text/title_text.dart';

import '../controllers/sign_up_controller.dart';

class SignUpView extends GetView<SignUpController> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: iconColor()),
        backgroundColor: Theme.of(context).colorScheme.background,
        title: AppBarText(
          title: 'sign_up'.tr,
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(left: 16, right: 16, top: 0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      buildTopImage(),
                      Container(
                        padding: EdgeInsets.all(16),
                        child: TitleText(
                          textSize: 18,
                          text: 'registration'.tr.toUpperCase(),
                        ),
                      ),
                      Form(
                        // key: controller.formKey,
                        child: Column(
                          children: [
                            TextInputLayout(
                              hintText: 'enter_name'.tr,
                              labelText: 'enter_name'.tr,
                              prefixIcon: Icon(Icons.assignment_ind),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.go,
                              autoValidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (v) {
                                // return controller.validateMobile(v!);
                              },
                            ),
                            TextInputLayout(
                              hintText: 'enter_email'.tr,
                              labelText: 'enter_email'.tr,
                              prefixIcon: Icon(Icons.email),
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.go,
                              autoValidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (v) {
                                // return controller.validateMobile(v!);
                              },
                            ),
                            TextInputLayout(
                              hintText: 'mobile_no'.tr,
                              labelText: 'mobile_no'.tr,
                              prefixIcon: Icon(Icons.phone),
                              keyboardType: TextInputType.phone,
                              textInputAction: TextInputAction.go,
                              obscureText: false,
                              autoValidateMode:
                                  AutovalidateMode.onUserInteraction,
                              maxLength: 10,
                              validator: (v) {
                                // return controller.validateMobile(v!);
                              },
                            ),
                            TextInputLayout(
                              hintText: 'password'.tr,
                              labelText: 'password'.tr,
                              prefixIcon: Icon(Icons.star),
                              obscureText: true,
                              textInputAction: TextInputAction.go,
                            ),
                            TextInputLayout(
                              hintText: 'confirm_password'.tr,
                              labelText: 'confirm_password'.tr,
                              prefixIcon: Icon(Icons.star),
                              obscureText: true,
                              textInputAction: TextInputAction.done,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            LargeButton(
                              onPressed: () {
                                controller.goToHome();
                              },
                              text: 'continue'.tr,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
