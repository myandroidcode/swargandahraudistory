import 'package:audio_service/audio_service.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/modules/home/views/home_view.dart';

class SignUpController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  void goToHome() {
    Get.to(AudioServiceWidget(child: HomeView()));
  }
}
