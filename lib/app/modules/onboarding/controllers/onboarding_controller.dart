import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/data/onboarding_data.dart';
import 'package:swargandhar/app/modules/onboarding/providers/onboarding_provider.dart';

class OnboardingController extends GetxController
    with StateMixin<OnBoardingData> {
  late Rx<OnBoardingData> onBoardingPages;
  final selectedPageIndex = 0.obs;
  bool get isLastPage => selectedPageIndex.value == 2;
  var pageViewController = PageController();

  forwardDirection() {
    if (isLastPage) {
    } else
      pageViewController.nextPage(
          duration: 300.milliseconds, curve: Curves.ease);
  }

  @override
  void onInit() {
    super.onInit();
    OnboardingProvider().getPagesData().then((resp) {
      change(resp, status: RxStatus.success());
      onBoardingPages = Rx<OnBoardingData>(resp);
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
