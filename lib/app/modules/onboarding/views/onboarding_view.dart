import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/customview/text/title_text.dart';

import '../controllers/onboarding_controller.dart';

class OnboardingView extends GetView<OnboardingController> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // appBar: AppBar(
        //   title: Text('OnboardingView'),
        //   centerTitle: true,
        // ),
        body: controller.obx(
          (data) => Stack(
            children: [
              PageView.builder(
                  controller: controller.pageViewController,
                  onPageChanged: controller.selectedPageIndex,
                  itemCount: data!.pages!.length,
                  itemBuilder: (ctx, index) {
                    var onBoardingData = data.pages![index];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          onBoardingData.image!,
                          height: 152,
                          width: 152,
                        ),
                        TitleText(
                          text: onBoardingData.title,
                          textSize: 26,
                        ),
                      ],
                    );
                  }),
              controller.obx(
                (data) => Positioned(
                  bottom: 20,
                  left: 20,
                  child: Row(
                    children: List.generate(
                        data!.pages!.length,
                        (index) => Obx(
                              () => Container(
                                margin: EdgeInsets.all(2),
                                // padding: EdgeInsets.all(10),
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    // borderRadius: BorderRadius.circular(8),
                                    color: controller.selectedPageIndex.value ==
                                            index
                                        ? Colors.red
                                        : Colors.grey),
                              ),
                            )),
                  ),
                ),
              ),
              Positioned(
                right: 20,
                bottom: 20,
                child: FloatingActionButton(
                    backgroundColor: Colors.indigoAccent,
                    elevation: 0,
                    onPressed: () {
                      controller.forwardDirection();
                    },
                    // onPressed: controller.forwardDirection(),
                    child: Obx(
                      () => TitleText(
                        text: controller.isLastPage ? 'Start' : 'Next',
                      ),
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
