import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/data/onboarding_data.dart';

class OnboardingProvider extends GetConnect {
  @override
  void onInit() {
    // httpClient.baseUrl = 'YOUR-API-URL';
    getPagesData();
  }

  Future<OnBoardingData> getPagesData() async {
    var response = await rootBundle.loadString('assets/json/onboarding.json');
    final data = await json.decode(response);
    return OnBoardingData.fromJson(data);
  }
}
