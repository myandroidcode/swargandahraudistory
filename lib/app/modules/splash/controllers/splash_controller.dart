import 'dart:async';

import 'package:audio_service/audio_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/modules/home/views/home_view.dart';
import 'package:swargandhar/app/routes/app_pages.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  startTimer() async {
    var duration = Duration(seconds: 1);
    return new Timer(duration, startNextScreen);
  }
}

void startNextScreen() {
  bool homeView = false;
  StreamBuilder<bool>(
    stream: AudioService.notificationClickEventStream,
    builder: (context, snapshot) {
      if (snapshot.data != null) {
        homeView = snapshot.data!;
      }
      return Container();
    },
  );
  if (homeView) {
    Get.to(AudioServiceWidget(child: HomeView()));
  } else {
    Get.offNamed(Routes.LOGIN);
  }
}
