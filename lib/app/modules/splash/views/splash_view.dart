import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/customview/appbar/custom_scaffold.dart';

import '../controllers/splash_controller.dart';

class SplashView extends GetView<SplashController> {
  @override
  Widget build(BuildContext context) {
    controller.startTimer();
    return CustomScaffold(
      // appBar: AppBar(
      //   title: Text('SplashView'),
      //   centerTitle: true,
      // ),
      body: Container(
        padding: EdgeInsets.all(16),
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //       colors: [orangeColors, orangeLightColors],
        //       end: Alignment.bottomCenter,
        //       begin: Alignment.topCenter),
        // ),
        child: Center(
          child: Image.asset("assets/images/splash_image.jpeg"),
        ),
      ),
    );
  }
}
