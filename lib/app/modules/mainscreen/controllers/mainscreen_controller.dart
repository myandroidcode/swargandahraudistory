import 'package:get/get.dart';
import 'package:swargandhar/app/storage/swargandhar_storage.dart';

class MainscreenController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  readLocal() {
    return SwargandharStorage.local();
  }

  changeTheme(bool value) {
    SwargandharStorage.saveBool('darkMode', value);
  }

  getThemeDarkMode() {
    return SwargandharStorage.getBool('darkMode');
  }
}
