import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/general/appThemes.dart';
import 'package:swargandhar/app/modules/network/bindings/network_binding.dart';
import 'package:swargandhar/app/routes/app_pages.dart';
import 'package:swargandhar/app/storage/storage_key.dart';
import 'package:swargandhar/app/storage/swargandhar_storage.dart';
import 'package:swargandhar/app/translations/app_translation.dart';

import '../controllers/mainscreen_controller.dart';

class MainScreenView extends GetView<MainscreenController> {
  @override
  Widget build(BuildContext context) {
    var controller = Get.put(MainscreenController());
    bool isDarkMode =
        SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE) ?? false;
    SwargandharStorage.writeBool(GetStorageKey.IS_DARK_MODE, isDarkMode);
    // print('store locals ${SwargandharStorage.local()}');
    return ThemeProvider(
      initTheme:
          isDarkMode ? AppThemes.darkThemeData : AppThemes.lightThemeData,
      child: Builder(
        builder: (context) => GetMaterialApp(
          title: "Application",
          debugShowCheckedModeBanner: false,
          initialRoute: AppPages.INITIAL,
          getPages: AppPages.routes,
          theme: ThemeProvider.of(context),
          darkTheme: AppThemes.darkThemeData,
          // defaultTransition: Transition.leftToRightWithFade,
          translations: AppTranslation(),
          locale: controller.readLocal() ?? Locale('en', 'IN'),
          fallbackLocale: Locale('en', 'IN'),
          initialBinding: NetworkBinding(),
        ),
      ),
    );
  }
}
