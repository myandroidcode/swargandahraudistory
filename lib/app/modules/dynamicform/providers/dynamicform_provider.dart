import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/data/dynamic_form.dart';

class DynamicformProvider extends GetConnect {
  @override
  void onInit() {
    // httpClient.baseUrl = 'YOUR-API-URL';
    getLocals();
  }

  Future<Dynamic> getLocals() async {
    final String response =
        await rootBundle.loadString('assets/json/fields.json');
    final data = await json.decode(response);
    return Dynamic.fromJson(data);
  }
}
