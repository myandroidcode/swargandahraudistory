import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/customview/button/large_button.dart';
import 'package:swargandhar/app/customview/input/text_inout_layout.dart';
import 'package:swargandhar/app/customview/multi_select_form_field/multiselect_formfield.dart';
import 'package:swargandhar/app/customview/popup_menu_btn/popup_menu.dart';
import 'package:swargandhar/app/customview/text/title_text.dart';
import 'package:swargandhar/app/data/dynamic_form.dart';

import '../controllers/dynamicform_controller.dart';
import 'multiselect_demo_view.dart';

class DynamicformView extends GetView<DynamicformController> {
  List? _myActivities;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: controller.obx((state) => Text(state!.data!.calculatorName!)),
        // centerTitle: true,
      ),
      // body: ListView(children: listWidget(controller.data)),
      body: controller.obx((data) => ListView(
            children: listWidget(data),
          )),
      // body: Column(
      //   children: [
      //     // TitleText(
      //     //   overflow: TextOverflow.visible,
      //     //   text: '${controller.localData['data'].toString()}',
      //     // ),
      //     // Divider(
      //     //   thickness: 4,
      //     //   color: Colors.red,
      //     //   height: 18,
      //     // ),
      //     // TitleText(
      //     //   overflow: TextOverflow.visible,
      //     //   text: '${controller.localData['data']['inputs']}',
      //     // ),
      //
      //     controller.obx((data) => ListView.builder(
      //           itemCount: 1,
      //           itemBuilder: (context, index) {
      //             return ListTile(
      //               title: TitleText(
      //                 text: '${data!.data!.inputs!.length}',
      //               ),
      //             );
      //           },
      //         )),
      //   ],
      // ),
    );
  }

  List<Widget> listWidget(Dynamic? data) {
    List<Widget> children = [];
    // children.add(addMultiSelectFormField());

    data!.data!.inputs!.forEach((input) {
      switch (input.label) {
        case 'Your Height':
          children.add(Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: TextInputLayout(
                  hintText: input.label,
                  labelText: input.label,
                  maxLength: 8,
                  controller: controller.heightController,
                  keyboardType: input.type == 'number'
                      ? TextInputType.number
                      : TextInputType.number,
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 4),
                  width: 120,
                  child: CustomDropDown<Option>(
                    controller: controller.optionController,
                    onSelected: (v) {
                      controller.optionController.text = v.label!;
                    },
                    list: input.units!.options!
                        .map((option) => PopupMenuItem<Option>(
                              value: option,
                              child: TitleText(
                                text: option.label,
                              ),
                            ))
                        .toList(),
                  ))
            ],
          ));
          break;
        case 'Your Hip Circumference':
          children.add(TextInputLayout(
            hintText: input.label,
            labelText: input.label,
            maxLength: 8,
            controller: controller.heightController,
            keyboardType: input.type == 'number'
                ? TextInputType.number
                : TextInputType.number,
          ));
          break;
      }
      print('element $input');
    });
    children.add(LargeButton(
      text: 'Show Multi select FullScreen ',
      onPressed: () {
        Get.to(MultiSelectDemoView(), fullscreenDialog: true);
      },
    ));
    // data.value.data!.inputs!.length;
    return children;
  }

  List? values = ["Value 1", "Value 2", "Value 3", "Value 4", "Value 5"];
  addMultiSelectFormField() {
    return Container(
      padding: EdgeInsets.all(16),
      child: MultiSelectFormField(
        deleteIcon: Icon(
          Icons.close,
          color: Colors.white,
        ),
        onTap: (v) {
          print('ontap $v');
          _myActivities!.remove(v);
          // setState(() {});
        },
        autovalidate: false,
        chipBackGroundColor: Colors.blue,
        chipLabelStyle:
            TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        dialogTextStyle: TextStyle(fontWeight: FontWeight.bold),
        checkBoxActiveColor: Colors.blue,
        checkBoxCheckColor: Colors.white,
        dialogShapeBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.0))),
        title: Text(
          "My workouts",
          style: TextStyle(fontSize: 16),
        ),
        validator: (value) {
          if (value == null || value.length == 0) {
            return 'Please select one or more options';
          }
          return null;
        },
        dataSource: values,
        // textField: 'display',
        // valueField: 'value',
        okButtonLabel: 'OK',
        cancelButtonLabel: 'CANCEL',
        hintWidget: Text('Please choose one or more'),
        initialValue: _myActivities,
        onSaved: (value) {
          if (value == null) return;
          // setState(() {
          _myActivities = value;
          // });
        },
      ),
    );
  }

  addPrefixIcon(List<Option> options) {}
}
