import 'package:flutter/material.dart';
import 'package:swargandhar/app/customview/appbar/app_bar_text.dart';
import 'package:swargandhar/app/customview/multi_select_form_field/multiselect_formfield.dart';

class MultiSelectDemoView extends StatefulWidget {
  @override
  _MultiSelectDemoViewState createState() => _MultiSelectDemoViewState();
}

class _MultiSelectDemoViewState extends State<MultiSelectDemoView> {
  List? values = ["Value 1", "Value 2", "Value 3", "Value 4", "Value 5"];
  List? _myActivities;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AppBarText(
          title: 'Multi select With chip view',
        ),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            child: MultiSelectFormField(
              deleteIcon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onTap: (v) {
                print('ontap $v');
                _myActivities!.remove(v);
                setState(() {});
              },
              autovalidate: false,
              chipBackGroundColor: Colors.blue,
              chipLabelStyle:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              dialogTextStyle: TextStyle(fontWeight: FontWeight.bold),
              checkBoxActiveColor: Colors.blue,
              checkBoxCheckColor: Colors.white,
              dialogShapeBorder: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(12.0))),
              title: Text(
                "My workouts",
                style: TextStyle(fontSize: 16),
              ),
              validator: (value) {
                if (value == null || value.length == 0) {
                  return 'Please select one or more options';
                }
                return null;
              },
              dataSource: values,
              // textField: 'display',
              // valueField: 'value',
              okButtonLabel: 'OK',
              cancelButtonLabel: 'CANCEL',
              hintWidget: Text('Please choose one or more'),
              initialValue: _myActivities,
              onSaved: (value) {
                if (value == null) return;
                setState(() {
                  _myActivities = value;
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
