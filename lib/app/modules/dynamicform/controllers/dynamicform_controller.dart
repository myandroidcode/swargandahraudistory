import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/data/dynamic_form.dart';
import 'package:swargandhar/app/modules/dynamicform/providers/dynamicform_provider.dart';

class DynamicformController extends GetxController with StateMixin<Dynamic> {
  Rx<Dynamic>? data;
  late TextEditingController heightController;
  late TextEditingController optionController;

  @override
  void onInit() {
    super.onInit();
    DynamicformProvider().getLocals().then((resp) {
      change(resp, status: RxStatus.success());
      data = Rx<Dynamic>(resp);
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
    heightController = TextEditingController();
    optionController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  // get localDataList => localData;

  @override
  void onClose() {}
}
