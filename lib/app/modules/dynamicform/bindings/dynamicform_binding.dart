import 'package:get/get.dart';
import 'package:swargandhar/app/modules/dynamicform/controllers/dynamicform_controller.dart';

class DynamicformBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DynamicformController>(
      () => DynamicformController(),
    );
  }
}
