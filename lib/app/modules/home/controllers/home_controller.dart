import 'package:audio_service/audio_service.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:rxdart/rxdart.dart';
import 'package:swargandhar/app/music_player/audio_player_task.dart';
import 'package:swargandhar/app/music_player/general_classes.dart';

class HomeController extends GetxController {
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  startBackgroundPlay() async {
    await AudioService.start(
        backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint);
  }

  void startAudioPlayer() {
    AudioService.start(
      backgroundTaskEntrypoint: _audioPlayerTaskEntrypoint,
      androidNotificationChannelName: 'Audio Service Demo',
      // Enable this if you want the Android service to exit the foreground state on pause.
      //androidStopForegroundOnPause: true,
      androidNotificationColor: 0xFF2196f3,
      androidNotificationIcon: 'mipmap/ic_launcher',
      androidEnableQueue: true,
    );
  }

  void _audioPlayerTaskEntrypoint() async {
    AudioServiceBackground.run(() => AudioPlayerTask());
  }

  Stream<MediaState> get mediaStateStream =>
      Rx.combineLatest2<MediaItem?, Duration, MediaState>(
          AudioService.currentMediaItemStream,
          AudioService.positionStream,
          (mediaItem, position) => MediaState(mediaItem, position));

  /// A stream reporting the combined state of the current queue and the current
  /// media item within that queue.
  Stream<QueueState> get queueStateStream =>
      Rx.combineLatest2<List<MediaItem>?, MediaItem?, QueueState>(
          AudioService.queueStream,
          AudioService.currentMediaItemStream,
          (queue, mediaItem) => QueueState(queue, mediaItem));

// void changeTheme(String v) {
//   if (v == 'dark') {
//     Get.changeTheme(AppTheme().darkTheme);
//     SwargandharStorage.saveBool('darkMode', true);
//   } else {
//     Get.changeTheme(AppTheme().lightTheme);
//     SwargandharStorage.saveBool('darkMode', false);
//   }
// }

}
