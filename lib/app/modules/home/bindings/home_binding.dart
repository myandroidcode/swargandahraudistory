import 'package:get/get.dart';
import 'package:swargandhar/app/modules/dynamicform/controllers/dynamicform_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<DynamicformController>(
      () => DynamicformController(),
    );
  }
}
