import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/constants/top_image.dart';
import 'package:swargandhar/app/customview/appbar/app_bar_text.dart';
import 'package:swargandhar/app/customview/appbar/custom_scaffold.dart';
import 'package:swargandhar/app/customview/button/large_button.dart';
import 'package:swargandhar/app/customview/input/text_inout_layout.dart';
import 'package:swargandhar/app/customview/text/sub_title.dart';
import 'package:swargandhar/app/customview/text/title_text.dart';
import 'package:swargandhar/app/general/appColors.dart';
import 'package:swargandhar/app/routes/app_pages.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.background,
        title: AppBarText(
          title: 'login_screen'.tr,
        ),
        actions: [
          IconButton(
              icon: controller.isDarkMode.value
                  ? Icon(
                      CupertinoIcons.settings,
                      color: AppColors().kPrimaryTextColor,
                    )
                  : Icon(
                      CupertinoIcons.settings_solid,
                      color: AppColors().kBlackColor,
                    ),
              onPressed: () {
                showLanguageBottomSheet(context);
              }),
          ThemeSwitcher(
            builder: (context) => Obx(
              () => IconButton(
                icon: controller.isDarkMode.value
                    ? Icon(CupertinoIcons.brightness)
                    : Icon(
                        CupertinoIcons.moon_stars,
                        color: AppColors().kBlackColor,
                      ),
                onPressed: () {
                  controller.changeTheme(context);
                },
              ),
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  buildTopImage(),
                  SizedBox(
                    height: 20,
                  ),
                  TitleText(
                    text: 'enter_mobile'.tr,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Form(
                    key: controller.formKey,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Column(
                      children: [
                        // _buildTextFormField(),
                        TextInputLayout(
                          controller: controller.mobileNoController,
                          hintText: 'mobile_no'.tr,
                          labelText: 'mobile_no'.tr,
                          prefixIcon: Icon(Icons.phone),
                          keyboardType: TextInputType.phone,
                          obscureText: false,
                          textInputAction: TextInputAction.go,
                          maxLength: 10,
                          validator: (v) {
                            return controller.validateMobile(v!);
                          },
                          onSaved: (v) {},
                        ),
                        TextInputLayout(
                          controller: controller.passwordController,
                          hintText: 'password'.tr,
                          labelText: 'password'.tr,
                          prefixIcon: Icon(Icons.star),
                          obscureText: true,
                          maxLength: 4,
                          keyboardType: TextInputType.number,
                          textInputAction: TextInputAction.done,
                          validator: (v) {
                            return controller.validatePin(v!);
                          },
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        LargeButton(
                          onPressed: () {
                            if (controller.login()) {
                              controller.goToHome();
                            }
                            // Get.snackbar(
                            //   "login",
                            //   "login fuctionality is "
                            //       "comming soon",
                            //   snackPosition: SnackPosition.BOTTOM,
                            // );
                          },
                          text: 'continue'.tr,
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        InkWell(
                          onTap: () {
                            Get.toNamed(Routes.SIGN_UP);
                          },
                          child: SubTitle(
                            height: 40,
                            text: 'register'.tr,
                            textSize: 26,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          // Align(
          //   alignment: Alignment.bottomRight,
          //   child: footerClipPath(Get.height),
          // )
        ],
      ),
    );
  }

  showLanguageBottomSheet(BuildContext context) {
    Get.bottomSheet(
      Container(
        padding: EdgeInsets.all(16),
        child: controller.obx((data) => ListView.separated(
              itemBuilder: (_, index) {
                return ListTile(
                  onTap: () {
                    Locale local = Locale(data![index]['languageCode'],
                        data[index]['countryCode']);
                    controller.updateLocal(local);
                  },
                  leading: CircleAvatar(
                    radius: 36.0,
                    backgroundColor: Theme.of(context).buttonColor,
                    child: TitleText(
                      text: data![index]['languageChar'].toString(),
                    ),
                  ),
                  title: TitleText(
                    text: data[index]['language'],
                  ),
                );
              },
              itemCount: data!.length,
              shrinkWrap: true,
              separatorBuilder: (BuildContext context, int index) {
                return Divider(
                  color: Colors.amber,
                );
              },
            )),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
    );
  }
}
