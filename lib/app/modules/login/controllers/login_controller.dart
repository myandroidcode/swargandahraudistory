import 'package:animated_theme_switcher/animated_theme_switcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:swargandhar/app/general/appThemes.dart';
import 'package:swargandhar/app/modules/login/providers/local_provider.dart';
import 'package:swargandhar/app/routes/app_pages.dart';
import 'package:swargandhar/app/storage/storage_key.dart';
import 'package:swargandhar/app/storage/swargandhar_storage.dart';

class LoginController extends GetxController with StateMixin<List<dynamic>> {
  late TextEditingController mobileNoController;
  late TextEditingController passwordController;

  late GlobalKey<FormState>? formKey;
  late TextEditingController? mobileNumberEditingController;
  bool isFormValid = false;
  var isDarkMode = false.obs;

  @override
  void onInit() {
    super.onInit();
    LocalProvider().getLocals().then((resp) {
      change(resp, status: RxStatus.success());
    }, onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    formKey = GlobalKey<FormState>();
    isDarkMode.value = SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE);
    mobileNoController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    mobileNoController.dispose();
    passwordController.dispose();
  }

  void updateLocal(Locale local) {
    SwargandharStorage.writeData(GetStorageKey.localLanguage, local.toString());
    Get.updateLocale(local);
    Get.back();
  }

  void goToHome() {
    Get.offNamed(Routes.HOME);
  }

  String? validateMobile(String value) {
    if (value.length < 10) {
      return "Provide valid mobile number";
    }
    return null;
  }

  String? validatePin(String value) {
    if (value.length < 4) {
      return "Please provide 4 digit pin";
    }
    return null;
  }

  login() {
    isFormValid = formKey!.currentState!.validate();
    if (!isFormValid) {
      return false;
    } else {
      formKey!.currentState!.save();
      return true;
    }
  }

  void changeTheme(BuildContext context) {
    final theme =
        Get.isDarkMode ? AppThemes.lightThemeData : AppThemes.darkThemeData;
    ThemeSwitcher.of(context)!.changeTheme(theme: theme);
    if (SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE)) {
      SwargandharStorage.writeBool(GetStorageKey.IS_DARK_MODE, false);
      isDarkMode.value = false;
    } else {
      SwargandharStorage.writeBool(GetStorageKey.IS_DARK_MODE, true);
      isDarkMode.value = true;
    }
  }
}
