import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';

class LocalProvider extends GetConnect {
  @override
  void onInit() {}

  Future<List<dynamic>> getLocals() async {
    final String response =
        await rootBundle.loadString('assets/json/local.json');
    final data = await json.decode(response);
    return data['locals'];
  }
}
