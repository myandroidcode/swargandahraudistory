import 'package:flutter/cupertino.dart';

class Dimen {
  static const BIG_TITLE_TEXT_SIZE = 24.0;
  static const BIG_SUB_TITLE_TEXT_SIZE = 16.0;
  static const TITLE_TEXT_SIZE = 16.0;
  static const SUB_TITLE_TEXT_SIZE = 14.0;
  static const DEPENDENT_CARD_WIDTH = 94.0;
  static const DEPENDENT_CARD_HEIGHT = 135.0;
  static const DEPENDENT_CARD_PADDING_VERTICAL = 12.0;
  static const DEPENDENT_CARD_PADDING_HORIZONTAL = 16.0;
  static const DEPENDENT_ADD_CARD_PADDING_BOTTOM = 11.0;
  static const DEPENDENT_CARD_IMAGE_SIZE = 70.0;
  static const DEPENDENT_SCREEN_SIDE_MARGIN = 30.0;
  static const PROFILE_IMAGE_SIZE = 98.0;
  static const FORM_MARGIN_TOP = 24.0;
  static const FOOTER_BUTTON_SIDE_MARGIN = 25.0;
  static const SCREEN_MARGIN_TOP_BOTTOM = 12.0;
  static const FORM_TITLE_MARGIN = 67.0;
  static const MATCH_PROFILE_IMAGE_SIZE = 56.0;
  static const IMAGE_MARGIN_TOP = 30.0;
  static const EMPTY_CONTAINER_HEIGHT = 28.0;
  static const EMPTY_CONTAINER_WIDTH = 28.0;
  static const SMALL_TEXT_SIZE = 12.0;
  static const ARROW_TOP_BOTTOM_PADDING = 18.0;
  static const ARROW_RIGHT_BOTTOM_PADDING = 12.0;
  static const POPUP_HEIGHT = 64.0;
  static const SCREEN_MARGIN_TOP = 25.0;
  static const SCREEN_MARGIN_SIDE = 20.0;
  static const FIND_DOCTOR_IMAGE_SIZE = 53.0;
  static const FIND_DOCTOR_IMAGE_MARGIN = 43.0;
  static const CHIP_WIDTH = 70.0;
  static const ARTICLE_IMAGE_SIZE = 172.0;
  static const SURVEY_SCREEN_SIDE_TOP_MARGIN = 64.0;
  static const SURVEY_SCREEN_SIDE_MARGIN = 40.0;
  static const HOME_PAGE_TITLE_TOP_PADDING = 20.0;
  static const HOME_PAGE_TITLE_BOTTOM_PADDING = 8.0;
  static var MAX_WIDTH = 360.0;

  static double getRatio(BuildContext context, double width) {
    return width / 360 * MediaQuery.of(context).size.width;
  }

  static double getHeightRatio(BuildContext context, double height) {
    return height / 640 * MediaQuery.of(context).size.height;
  }

  static double getWidthRatio(BuildContext context, double width) {
    return width / 360 * MediaQuery.of(context).size.width;
  }


  static double getHeightPercentage(BuildContext context, double height) {
    return (height * MediaQuery.of(context).size.height)/100;
  }
}
