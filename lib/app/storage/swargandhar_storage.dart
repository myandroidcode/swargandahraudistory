import 'dart:ui';

import 'package:get_storage/get_storage.dart';
import 'package:swargandhar/app/storage/storage_key.dart';

class SwargandharStorage {
  static final storage = GetStorage('MyPref');

  static writeData(String key, String value) {
    storage.write(key, value);
  }

  static writeBool(String key, bool value) {
    storage.write(key, value);
  }

  static saveString(String key, String value) {
    storage.write(key, value);
  }

  static saveBool(String key, bool value) {
    storage.write(key, value);
  }

  static getBool(String key) {
    return storage.read(key) ?? false;
  }

  // static readString(String key) {
  //   storage.read(key);
  // }
  static readData(String key) {
    return storage.read(key);
  }

  static local() {
    String? locals = SwargandharStorage.readData(GetStorageKey.localLanguage);
    if (locals != null) {
      var splits = locals.split("_");
      // print('locals $locals');
      // print('first part ${splits[0]}');
      // print('second part ${splits[1]}');
      return Locale(splits[0].toString(), splits[1].toString());
    }
    return Locale('en', 'IN');
  }

  static remove(String key) {
    storage.remove(key);
  }

  static deleateAllData() {
    storage.erase();
  }
}
