import 'package:flutter/material.dart';
import 'package:swargandhar/app/general/appColors.dart';

import 'footer_background.dart';

Widget footerClipPath(double? height) => ClipPath(
      clipper: FooterWaveClipper(),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: AppColors().bottomFooterGradient,
              begin: Alignment.center,
              end: Alignment.bottomRight),
        ),
        height: height! / 3,
      ),
    );
