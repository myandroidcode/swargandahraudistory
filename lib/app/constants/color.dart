import 'package:flutter/material.dart';
import 'package:swargandhar/app/storage/storage_key.dart';
import 'package:swargandhar/app/storage/swargandhar_storage.dart';

Color orangeColors = Color(0xffF5591F);
Color orangeLightColors = Color(0xffF2861E);
Color white = Color(0xDDFFFFFF);
Color black = Color(0xDD000000);

Color iconColor() {
  bool isDarkMode =
      SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE) ?? false;
  SwargandharStorage.writeBool(GetStorageKey.IS_DARK_MODE, isDarkMode);
  return isDarkMode ? white : black;
}
