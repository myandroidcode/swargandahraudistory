import 'package:flutter/material.dart';

Widget buildTopImage() {
  return Container(
    margin: EdgeInsets.only(top: 26),
    child: Image.asset(
      'assets/images/splash_image.jpeg',
      height: 150,
      width: 150,
    ),
  );
}
