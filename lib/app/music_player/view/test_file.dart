// import 'dart:convert';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
//
// class DropDown extends StatefulWidget {
//   @override
//   _DropDownState createState() => _DropDownState();
// }
//
// class _DropDownState extends State<DropDown> {
//   String arrayObject = """
// {"error":false,"modules":[{"module_id":1003,"module_name":"Market Ideas","document_params":[{"code":"title","label":"Title","type":"Text","required":true},{"code":"category","label":"Category","type":"List","options":["Ideas","Competitor Updates"],"required":true},{"code":"description","label":"Description","type":"Text","required":true},{"code":"attachments","label":"Attachments","type":"file_image","required":false}],"tenant_id":100},{"module_id":1001,"module_name":"Expense","document_params":[{"code":"claim_date","label":"Claim Date","type":"date","required":true},{"code":"claim_value","label":"Claim Value","type":"number","required":true},{"code":"category","label":"Category","type":"List","options":["Travel Allowance","Petrol Allowance","Food Allowance","Accommodation Allowance","Other Allowance"],"required":true},{"code":"notes","label":"Notes","type":"Text","required":true},{"code":"attachments","label":"Attachments","type":"file_image","required":false}],"tenant_id":100},{"module_id":1002,"module_name":"Software Issues","document_params":[{"code":"title","label":"Title","type":"Text","required":true},{"code":"description","label":"Description","type":"Text","required":true},{"code":"date_of_occurrence","label":"Date of Occurrence","type":"date","required":true},{"code":"attachments","label":"Attachments","type":"file_image","required":false}],"tenant_id":100}]} """;
//   List<Modules> module = [];
//
//   //List<DropdownMenuItem<Modules>> _dropdownMenuItems1;
//   late Modules selectedItems;
//   late List<String> options;
//   int i = 0;
//
//   @override
//   void initState() {
//     super.initState();
//     final details = jsonDecode(arrayObject)["modules"] as List;
//     module =
//         details.map((customerjson) => Modules.fromJson(customerjson)).toList();
//     /*  _dropdownMenuItems1 = buildDropDownMenuItems(module);
//     selectedItems = _dropdownMenuItems1[0].value;*/
//     print(details);
//   }
//
// /* List<DropdownMenuItem<Modules>> buildDropDownMenuItems(List listItems) {
//     List<DropdownMenuItem<Modules>> items = List();
//     for (Modules listItem in listItems) {
//       items.add(
//         DropdownMenuItem(
//          child: Text(listItem.moduleName),
//           value: listItem,
//         ),
//       );
//     }
//     return items;
//   }*/
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("DropDownList"),
//       ),
//       body: SafeArea(
//         child: _firstContent(),
//       ),
//     );
//   }
//
//   _firstContent() {
//     return SingleChildScrollView(
//         child: Column(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Padding(padding: EdgeInsets.all(16)),
//               Text(
//                 'Modules',
//                 style: TextStyle(
//                     fontSize: 15, color: Colors.black, fontWeight: FontWeight.bold),
//                 textAlign: TextAlign.center,
//               ),
//               Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       margin: const EdgeInsets.all(10.0),
//                       padding: const EdgeInsets.all(3.0),
//                       decoration:
//                       BoxDecoration(border: Border.all(color: Colors.black)),
//                       child: DropdownButton(
//                         hint: Text('Select Modules'),
//                         dropdownColor: Colors.white,
//                         focusColor: Colors.black12,
//                         icon: Icon(Icons.arrow_drop_down),
//                         iconSize: 30,
//                         isExpanded: true,
//                         underline: SizedBox(),
//                         style: TextStyle(color: Colors.black, fontSize: 22),
//                         value: selectedItems,
//                         items: //_dropdownMenuItems1,
//                         module.map((selecteditems) {
//                           return DropdownMenuItem(
//                             value: selecteditems,
//                             child: Text(selecteditems.moduleName),
//
//                           );
//                         }).toList(),
//                         onChanged: (newvalue) {
//                           setState(() {
//                             /*_dropdownMenuItems1 = buildDropDownMenuItems(module);
//                         selectedItems = _dropdownMenuItems1[0].value;*/
//                             selectedItems = newvalue;
//                           });
//                           print(selectedItems.moduleName);
//                         },
//                         /* hint: Text(
//                       "Please choose a langauage",
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontSize: 14,
//                           fontWeight: FontWeight.w500),
//                     ),*/
//                       ),
//                     ),
//                     /* Container(
//                     child: selectedItems != null
//                         ? selectedItems.moduleName == "Market Ideas"
//                             ? MarketIdeasFormWidget()
//                             : selectedItems.moduleName == "Expense"
//                                 ? ExpenseFormWidget()
//                                 : SoftWareIssuesFormWidget()
//                         : getvalues()),*/
//                   ])
//             ]));
//   }
//
//   Widget getvalues() {
//     Text(" ");
//   }
//
//   Widget MarketIdeasFormWidget() {
//     return Container(
//         child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               Container(
//                   child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Padding(
//                           padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
//                         ),
//                       ])),
//               Container(
//                   margin: const EdgeInsets.all(10.0),
//                   padding: const EdgeInsets.all(3.0),
//                   decoration: BoxDecoration(),
//                   child: Row(children: <Widget>[
//                     Flexible(
//                       child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               'Titte',
//                               style: TextStyle(
//                                   fontSize: 15,
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold),
//                               textAlign: TextAlign.center,
//                             ),
//                             SizedBox(
//                               height: 20,
//                             ),
//                             TextFormField(
//                               decoration: InputDecoration(
//                                   hintText: 'Titte',
//                                   border: new OutlineInputBorder(
//                                     borderRadius: BorderRadius.all(Radius.circular(
//                                         5.0) //                 <--- border radius here
//                                     ),
//                                   )),
//                               /* labelText: "Tittle *",
//                         labelStyle: TextStyle(
// fontSize: 20,
//                           color: Colors.black,
//                         ),*/
//                             ),
//                             SizedBox(
//                               height: 20.0,
//                             ),
//                             Text(
//                               'Category',
//                               style: TextStyle(
//                                   fontSize: 15,
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: <Widget>[
//                                   Container(
//                                     margin: const EdgeInsets.all(10.0),
//                                     padding: const EdgeInsets.all(3.0),
//                                     decoration: BoxDecoration(
//                                         border: Border.all(color: Colors.black)),
//                                     child: DropdownButton(
//                                       hint: Text('Category'),
//                                       dropdownColor: Colors.white,
//                                       icon: Icon(Icons.arrow_drop_down),
//                                       iconSize: 30,
//                                       isExpanded: true,
//                                       underline: SizedBox(),
//                                       style: TextStyle(
//                                           color: Colors.black, fontSize: 22),
//                                       value: options,
//                                       onChanged: (newvalue) {
//                                         setState(() {
//                                           options = newvalue;
//                                         });
//                                       },
//                                       items: module.map((valueItem) {
//                                         if (valueItem.documentParams[1].options !=
//                                             null) {
//                                           return DropdownMenuItem(
//                                             value: valueItem,
//                                             child: Text(valueItem
//                                                 .documentParams[1].options[i++]),
//                                           );
//                                         }
//                                       }).toList(),
//                                     ),
//                                   ),
//                                   Text(
//                                     'Description',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   Padding(
//                                       padding: EdgeInsets.fromLTRB(10, 10, 10, 10)),
//                                   TextFormField(
//                                     decoration: InputDecoration(
//                                       enabledBorder: OutlineInputBorder(
//                                           borderSide:
//                                           BorderSide(color: Colors.black)),
//                                       hintText: 'Description',
//                                       //labelText:"${selectedItems.documentParams[0].options}",
//                                     ),
//                                   ),
//                                   Padding(
//                                       padding: EdgeInsets.fromLTRB(10, 10, 10, 10)),
//                                   Text(
//                                     'Attachments',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   ListTile(
//                                       leading: Icon(Icons.add),
//                                       title: Text("Attachments"),
//                                       onTap: () async {
//                                         FilePickerResult result =
//                                         await FilePicker.platform.pickFiles();
//                                         if (result != null) {
//                                           Share.shareFiles(
//                                               ["${result.files.single.path}"]);
//                                         }
//                                       }),
//                                 ])
//                           ]),
//                     )
//                   ]))
//             ]));
//   }
//
//   Widget ExpenseFormWidget() {
//     return SingleChildScrollView(
//         child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               Container(
//                   child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Padding(
//                           padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
//                         ),
//                       ])),
//               Container(
//                   margin: const EdgeInsets.all(10.0),
//                   padding: const EdgeInsets.all(3.0),
//                   decoration: BoxDecoration(),
//                   child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <
//                       Widget>[
//                     Flexible(
//                       child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: <Widget>[
//                             Text(
//                               'claim_date',
//                               style: TextStyle(
//                                   fontSize: 15,
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             Column(children: <Widget>[
//                               DateTimePicker(
//                                 initialValue: '',
//                                 firstDate: DateTime(2000),
//                                 lastDate: DateTime(2100),
//                                 icon: Icon(Icons.event),
//                                 dateLabelText: 'Date',
//                                 onChanged: (val) => print(val),
//                                 validator: (val) {
//                                   print(val);
//                                   return null;
//                                 },
//                                 onSaved: (val) => print(val),
//                               )
//                             ]),
//                             SizedBox(
//                               height: 20.0,
//                             ),
//                             Text(
//                               'claim_date',
//                               style: TextStyle(
//                                   fontSize: 15,
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             TextFormField(
//                               decoration: InputDecoration(
//                                 enabledBorder: OutlineInputBorder(
//                                     borderSide: BorderSide(color: Colors.black)),
//                                 hintText: 'claim_value',
//                                 // labelText: "${selectedItems.documentParams[0].code}",
//                                 /*  border: OutlineInputBorder(
//                             borderRadius: BorderRadius.circular(30.0),
//                             borderSide: BorderSide.none),
//                         filled: true,
//                         fillColor: Colors.grey.withOpacity(0.1),*/
//                               ),
//                               keyboardType: TextInputType.number,
//                             ),
//                             SizedBox(
//                               height: 20.0,
//                             ),
//                             Text(
//                               'Category',
//                               style: TextStyle(
//                                   fontSize: 15,
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: <Widget>[
//                                   Container(
//                                     padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
//                                     decoration: BoxDecoration(
//                                       border: Border.all(color: Colors.black),
//                                       shape: BoxShape.rectangle,
//                                     ),
//                                     child: DropdownButton(
//                                       hint: Text('Category'),
//                                       dropdownColor: Colors.white,
//                                       icon: Icon(Icons.arrow_drop_down),
//                                       iconSize: 30,
//                                       isExpanded: true,
//                                       underline: SizedBox(),
//                                       style: TextStyle(
//                                           color: Colors.black, fontSize: 22),
//                                       value: options,
//                                       onChanged: (newvalue) {
//                                         setState(() {
//                                           options = newvalue;
//                                         });
//                                       },
//                                       items: module.map((valueItem) {
//                                         if (valueItem.documentParams[1].options !=
//                                             null) {
//                                           return DropdownMenuItem(
//                                             value: valueItem,
//                                             child: Text(selectedItems
//                                                 .documentParams[2].options
//                                                 .toString()),
//                                           );
//                                         }
//                                       }).toList(),
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: 20.0,
//                                   ),
//                                   Text(
//                                     'Notes',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   TextFormField(
//                                     decoration: InputDecoration(
//                                       enabledBorder: OutlineInputBorder(
//                                           borderSide:
//                                           BorderSide(color: Colors.black)),
//                                       hintText: 'Notes',
//                                       // labelText: "${selectedItems.documentParams[0].code}",
//                                       /* border: OutlineInputBorder(
//                                   borderRadius: BorderRadius.circular(30.0),
//                                   borderSide: BorderSide.none),
//                               filled: true,
//                               fillColor: Colors.grey.withOpacity(0.1),*/
//                                     ),
//                                   ),
//                                   Padding(padding: EdgeInsets.only(top: 12.0)),
//                                   Text(
//                                     'Attachments',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   ListTile(
//                                       leading: Icon(Icons.add),
//                                       title: Text("Attachments"),
//                                       onTap: () async {
//                                         FilePickerResult result =
//                                         await FilePicker.platform.pickFiles();
//                                         if (result != null) {
//                                           Share.shareFiles(
//                                               ["${result.files.single.path}"]);
//                                         }
//                                       }),
//                                 ])
//                           ]),
//                     )
//                   ]))
//             ]));
//   }
//
//   Widget SoftWareIssuesFormWidget() {
//     return Container(
//         child: Column(
//             mainAxisSize: MainAxisSize.min,
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: <Widget>[
//               Container(
//                   child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Padding(
//                           padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
//                         ),
//                       ])),
//               Container(
//                   margin: const EdgeInsets.all(10.0),
//                   padding: const EdgeInsets.all(3.0),
//                   decoration: BoxDecoration(),
//                   child: Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       children: <Widget>[
//                         Flexible(
//                             child: Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: <Widget>[
//                                   SizedBox(
//                                     height: 20.0,
//                                   ),
//                                   Text(
//                                     'Title',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   TextFormField(
//                                     decoration: InputDecoration(
//                                       enabledBorder: OutlineInputBorder(
//                                           borderSide: BorderSide(color: Colors.black)),
//                                       hintText: 'Title',
//                                       // labelText: "${selectedItems.documentParams[0].code}",
//                                       /* border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(30.0),
//                           borderSide: BorderSide.none),
//                       filled: true,
//                       fillColor: Colors.grey.withOpacity(0.1),*/
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: 20.0,
//                                   ),
//                                   Text(
//                                     'Description',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   TextFormField(
//                                     decoration: InputDecoration(
//                                       enabledBorder: OutlineInputBorder(
//                                           borderSide: BorderSide(color: Colors.black)),
//                                       hintText: 'Description',
//                                       //labelText: "${selectedItems.documentParams[0].label}",
//                                       /* border: OutlineInputBorder(
//                           borderRadius: BorderRadius.circular(30.0),
//                           borderSide: BorderSide.none),
//                       filled: true,
//                       fillColor: Colors.grey.withOpacity(0.1),*/
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: 20.0,
//                                   ),
//                                   Text(
//                                     'date_of_occurrence',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   Column(children: <Widget>[
//                                     DateTimePicker(
//                                       initialValue: '',
//                                       firstDate: DateTime(2000),
//                                       lastDate: DateTime(2100),
//                                       icon: Icon(Icons.event),
//                                       dateLabelText: 'Date',
//                                       onChanged: (val) => print(val),
//                                       validator: (val) {
//                                         print(val);
//                                         return null;
//                                       },
//                                       onSaved: (val) => print(val),
//                                     )
//                                   ]),
//                                   Padding(padding: EdgeInsets.only(top: 12.0)),
//                                   Text(
//                                     'Attachments',
//                                     style: TextStyle(
//                                         fontSize: 15,
//                                         color: Colors.black,
//                                         fontWeight: FontWeight.bold),
//                                   ),
//                                   ListTile(
//                                       leading: Icon(Icons.add),
//                                       title: Text("Attachments"),
//                                       onTap: () async {
//                                         FilePickerResult result =
//                                         await FilePicker.platform.pickFiles();
//                                         if (result != null) {
//                                           Share.shareFiles(
//                                               ["${result.files.single.path}"]);
//                                         }
//                                       }),
//                                 ])),
//                       ]))
//             ]));
//   }
// }
// Bro
// Dilip
// Ok i  am checking and revert back
// anand
// Tz u so much bro
// class DropDown {
//   bool error;
//   List<Modules> modules;
//
//   DropDown({
//     this.error,
//     this.modules,
//   });
//
//   factory DropDown.fromJson(Map<String, dynamic> json) => DropDown(
//     error: json["error"],
//     modules: List<Modules>.from(json["modules"].map((x) => Modules.fromJson(x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "error": error,
//     "modules": List<dynamic>.from(modules.map((x) => x.toJson())),
//   };
//
//   @override
//   String toString() {
//     return 'DropDown{error: $error, modules: $modules}';
//   }
// }
//
// class Modules {
//   int moduleId;
//   String moduleName;
//   List<DocumentParam> documentParams;
//   int tenantId;
//
//   Modules({
//     this.moduleId,
//     this.moduleName,
//     this.documentParams,
//     this.tenantId,
//   });
//
//
//   factory Modules.fromJson(Map<String, dynamic> json) => Modules(
//     moduleId: json["module_id"],
//     moduleName: json["module_name"],
//     documentParams: List<DocumentParam>.from(json["document_params"].map((x) => DocumentParam.fromJson(x))),
//     tenantId: json["tenant_id"],
//   );
//
//
//   Map<String, dynamic> toJson() => {
//     "module_id": moduleId,
//     "module_name": moduleName,
//     "document_params": List<dynamic>.from(documentParams.map((x) => x.toJson())),
//     "tenant_id": tenantId,
//   };
//
//   @override
//   String toString() {
//     return 'Module{moduleId: $moduleId, moduleName: $moduleName, documentParams: $documentParams, tenantId: $tenantId}';
//   }
// }
//
// class DocumentParam {
//   String code;
//   String label;
//   String type;
//   bool required;
//   List<String> options;
//
//   DocumentParam({
//     this.code,
//     this.label,
//     this.type,
//     this.required,
//     this.options,
//   });
//
//   factory DocumentParam.fromJson(Map<String, dynamic> json) => DocumentParam(
//     code: json["code"],
//     label: json["label"],
//     type: json["type"],
//     required: json["required"],
//     (options: json["options"] == null ? null : List<String>.from(json["options"].map((x) => x))),
//   );
//
//   Map<String, dynamic> toJson() => {
//     "code": code,
//     "label": label,
//     "type": type,
//     "required": required,
//     "options": options == null ? null : List<dynamic>.from(options.map((x) => x)),
//   };
//
//   @override
//   String toString() {
//     return 'DocumentParam{code: $code, label: $label, type: $type, required: $required, options: $options}';
//   }
// }
