import 'package:flutter/material.dart';
import 'package:swargandhar/app/storage/storage_key.dart';
import 'package:swargandhar/app/storage/swargandhar_storage.dart';

class AppColors {
  var bottomFooterGradient =
      SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE)
          ? [
              Colors.lightBlue,
              Colors.lightBlue.shade100,
            ]
          : [
              Color(0xFF6200EE),
              Colors.deepPurple.shade300,
            ];

  var kPrimaryTextColor = SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE)
      ? Color(0xDDFFFFFF)
      : Color(0xDD000000);
  var kSecondaryTextColor =
      SwargandharStorage.getBool(GetStorageKey.IS_DARK_MODE)
          ? Color(0x89FFFFFF)
          : Color(0x89000000);
  var kBlackColor = Colors.black;
}
