import 'package:google_fonts/google_fonts.dart';

import 'appColors.dart';

class AppTextStyles {
  var kTextStyleWithFont = GoogleFonts.montserrat();
  var appBarThemeColor = GoogleFonts.montserrat(
      fontSize: 20, color: AppColors().kPrimaryTextColor);
  var kTextStyleFourteenWithThemeColor = GoogleFonts.montserrat(
      fontSize: 14, color: AppColors().kPrimaryTextColor);

  var kTextStyleTwelveWithGreyColor = GoogleFonts.montserrat(
      fontSize: 12, color: AppColors().kSecondaryTextColor);

  titleStyle({double? fontSize}) {
    return GoogleFonts.montserrat(
        fontSize: fontSize ?? 14, color: AppColors().kPrimaryTextColor);
  }

  subTitleStyle({double? fontSize}) {
    return GoogleFonts.montserrat(
        fontSize: fontSize ?? 12, color: AppColors().kSecondaryTextColor);
  }
}
