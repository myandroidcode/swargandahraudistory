// To parse this JSON data, do
//
//     final onBoardingData = onBoardingDataFromJson(jsonString);

import 'dart:convert';

OnBoardingData onBoardingDataFromJson(String str) =>
    OnBoardingData.fromJson(json.decode(str));

String onBoardingDataToJson(OnBoardingData data) => json.encode(data.toJson());

class OnBoardingData {
  OnBoardingData({
    this.pages,
  });

  List<Page>? pages;

  factory OnBoardingData.fromJson(Map<String, dynamic> json) => OnBoardingData(
        pages: List<Page>.from(json["pages"].map((x) => Page.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "pages": List<dynamic>.from(pages!.map((x) => x.toJson())),
      };
}

class Page {
  Page({
    this.title,
    this.image,
    this.description,
  });

  String? title;
  String? image;
  String? description;

  factory Page.fromJson(Map<String, dynamic> json) => Page(
        title: json["title"],
        image: json["Image"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "Image": image,
        "description": description,
      };
}
