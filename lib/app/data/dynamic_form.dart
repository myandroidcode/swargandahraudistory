// To parse this JSON data, do
//
//     final dynamic = dynamicFromJson(jsonString);

import 'dart:convert';

Dynamic dynamicFromJson(String str) => Dynamic.fromJson(json.decode(str));

String dynamicToJson(Dynamic data) => json.encode(data.toJson());

class Dynamic {
  Dynamic({
    this.error,
    this.data,
  });

  bool? error;
  Data? data;

  factory Dynamic.fromJson(Map<String, dynamic> json) => Dynamic(
        error: json["error"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.calculatorName,
    this.action,
    this.method,
    this.inputs,
    this.chipDropDownValue,
  });

  String? calculatorName;
  String? action;
  String? method;
  List<Input>? inputs;
  List<String>? chipDropDownValue;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        calculatorName: json["calculator_name"],
        action: json["action"],
        method: json["method"],
        inputs: List<Input>.from(json["inputs"].map((x) => Input.fromJson(x))),
        chipDropDownValue:
            List<String>.from(json["chipDropDownValue"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "calculator_name": calculatorName,
        "action": action,
        "method": method,
        "inputs": List<dynamic>.from(inputs!.map((x) => x.toJson())),
        "chipDropDownValue":
            List<dynamic>.from(chipDropDownValue!.map((x) => x)),
      };
}

class Input {
  Input({
    this.label,
    this.type,
    this.key,
    this.required,
    this.units,
    this.options,
  });

  String? label;
  String? type;
  String? key;
  bool? required;
  Units? units;
  List<Option>? options;

  factory Input.fromJson(Map<String, dynamic> json) => Input(
        label: json["label"],
        type: json["type"],
        key: json["key"],
        required: json["required"],
        units: json["units"] == null ? null : Units.fromJson(json["units"]),
        options: json["options"] == null
            ? null
            : List<Option>.from(json["options"].map((x) => Option.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "label": label,
        "type": type,
        "key": key,
        "required": required,
        "units": units == null ? null : units!.toJson(),
        "options": options == null
            ? null
            : List<dynamic>.from(options!.map((x) => x.toJson())),
      };
}

class Option {
  Option({
    this.label,
    this.value,
    this.minimum,
    this.maximum,
    this.exclusiveMaximum,
  });

  String? label;
  String? value;
  int? minimum;
  int? maximum;
  bool? exclusiveMaximum;

  factory Option.fromJson(Map<String, dynamic> json) => Option(
        label: json["label"],
        value: json["value"],
        minimum: json["minimum"],
        maximum: json["maximum"],
        exclusiveMaximum: json["exclusiveMaximum"],
      );

  Map<String, dynamic> toJson() => {
        "label": label,
        "value": value,
        "minimum": minimum,
        "maximum": maximum,
        "exclusiveMaximum": exclusiveMaximum,
      };
}

class Units {
  Units({
    this.type,
    this.key,
    this.defaultValue,
    this.required,
    this.options,
  });

  String? type;
  String? key;
  String? defaultValue;
  bool? required;
  List<Option>? options;

  factory Units.fromJson(Map<String, dynamic> json) => Units(
        type: json["type"],
        key: json["key"],
        defaultValue: json["defaultValue"],
        required: json["required"],
        options:
            List<Option>.from(json["options"].map((x) => Option.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "key": key,
        "defaultValue": defaultValue,
        "required": required,
        "options": List<dynamic>.from(options!.map((x) => x.toJson())),
      };
}
