class FieldType {
  final String title;
  final int code;

  static const FieldType textBox = FieldType('textBox', 1);
  static const FieldType multiLineTextBox = FieldType('multiLineTextBox', 2);
  static const FieldType multiSelection = FieldType('multi_selection', 3);
  static const FieldType dropDown = FieldType('textBox', 4);
  static const FieldType imageUpload = FieldType('textBox', 5);
  static const FieldType datePicker = FieldType('textBox', 6);

  const FieldType(this.title, this.code);

  static FieldType? findByCode(int code) {
    for (var fieldType in values) {
      if (fieldType.code == code) {
        return fieldType;
      }
    }
    return null;
  }

  static var values = [
    textBox,
    multiLineTextBox,
    multiSelection,
    dropDown,
    imageUpload,
    datePicker
  ];
}
